# Villagers
execute as @e[type=minecraft:villager] run function starbidou_villtrades:villagers/tick

# Hero of the village
execute as @a[nbt={ActiveEffects: [{Id: 32}]}] run function starbidou_villtrades:hero/tick

# Clean villagers
tag @e[tag=starvillager_updated] remove starvillager_updated