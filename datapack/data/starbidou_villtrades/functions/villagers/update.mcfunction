# Add updated tags
tag @s add starvillager_updated
tag @s add starvillager_v0

# Resets level
tag @s remove starvillager_lv1
tag @s remove starvillager_lv2
tag @s remove starvillager_lv3
tag @s remove starvillager_lv4
tag @s remove starvillager_lv5
execute if entity @s[nbt={VillagerData: {level: 1}}] run tag @s add starvillager_lv1
execute if entity @s[nbt={VillagerData: {level: 2}}] run tag @s add starvillager_lv2
execute if entity @s[nbt={VillagerData: {level: 3}}] run tag @s add starvillager_lv3
execute if entity @s[nbt={VillagerData: {level: 4}}] run tag @s add starvillager_lv4
execute if entity @s[nbt={VillagerData: {level: 5}}] run tag @s add starvillager_lv5

# Job dependent update
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:armorer"}}] run function starbidou_villtrades:villagers/trades/armorer
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:butcher"}}] run function starbidou_villtrades:villagers/trades/butcher
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:cartographer"}}] run function starbidou_villtrades:villagers/trades/cartographer
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:cleric"}}] run function starbidou_villtrades:villagers/trades/cleric
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:farmer"}}] run function starbidou_villtrades:villagers/trades/farmer
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:fisherman"}}] run function starbidou_villtrades:villagers/trades/fisherman
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:fletcher"}}] run function starbidou_villtrades:villagers/trades/fletcher
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:leatherworker"}}] run function starbidou_villtrades:villagers/trades/leatherworker
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:librarian"}}] run function starbidou_villtrades:villagers/trades/librarian
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:mason"}}] run function starbidou_villtrades:villagers/trades/mason
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:shepherd"}}] run function starbidou_villtrades:villagers/trades/shepherd
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:toolsmith"}}] run function starbidou_villtrades:villagers/trades/toolsmith
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:weaponsmith"}}] run function starbidou_villtrades:villagers/trades/weaponsmith

execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:none"}}] run function starbidou_villtrades:villagers/trades/none
execute as @s if entity @s[nbt={VillagerData: {profession: "minecraft:nitwit"}}] run function starbidou_villtrades:villagers/trades/none