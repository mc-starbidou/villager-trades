# New villagers
execute if entity @s[tag=!starvillager_v0,tag=!starvillager_updated] run function starbidou_villtrades:villagers/update

# Villagers who lost their job
execute if entity @s[nbt={VillagerData: {profession: "minecraft:none"}},tag=starvillager_lv1,tag=!starvillager_updated] run function starbidou_villtrades:villagers/update

# Villager whose level has changed
execute if entity @s[nbt={VillagerData: {level: 1}, Offers: {Recipes: [{rewardExp: 1b}]}},tag=!starvillager_lv1,tag=!starvillager_updated] run function starbidou_villtrades:villagers/update
execute if entity @s[nbt={VillagerData: {level: 2}, Offers: {Recipes: [{rewardExp: 1b}]}},tag=!starvillager_lv2,tag=!starvillager_updated] run function starbidou_villtrades:villagers/update
execute if entity @s[nbt={VillagerData: {level: 3}, Offers: {Recipes: [{rewardExp: 1b}]}},tag=!starvillager_lv3,tag=!starvillager_updated] run function starbidou_villtrades:villagers/update
execute if entity @s[nbt={VillagerData: {level: 4}, Offers: {Recipes: [{rewardExp: 1b}]}},tag=!starvillager_lv4,tag=!starvillager_updated] run function starbidou_villtrades:villagers/update
execute if entity @s[nbt={VillagerData: {level: 5}, Offers: {Recipes: [{rewardExp: 1b}]}},tag=!starvillager_lv5,tag=!starvillager_updated] run function starbidou_villtrades:villagers/update
